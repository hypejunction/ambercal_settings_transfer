Plugin Settings Transfer
========================

Easily transfer plugin settings between multiple Elgg installations

## Features

* Easy to read JSON export
* Ability to manually edit plugins settings before import
* Import/export plugin settings
* Import/export plugin order
* Import/export enabled/disabled status

## Notes

The plugin does it's best to resolve the plugin dependencies, but it is recommended that
you use it on sites that have identical plugin set.
The plugin is intended primarily for transferring data between site clones, e.g.
a development and a production instance with the same code base.

## Acknowledgements

The plugin has been commissioned and partially sponsored by Ambercal

## Installation

If downloading from GitHub:

```sh
# install dependencies
composer install
```
